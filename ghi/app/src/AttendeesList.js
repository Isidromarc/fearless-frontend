import {React, useEffect, useState} from 'react';

function AttendeesList(props){
    const [attendees, setAttendees] = useState([]);

    const fetchAttendees = async () => {
        const attendeesURL = 'http://localhost:8001/api/attendees/';
        const attendeeResponse = await fetch(attendeesURL);
        if (attendeeResponse.ok){
            const data = await attendeeResponse.json();
            setAttendees(data.attendees);
        }
    }

    useEffect(() => {
        fetchAttendees();
    }, []);

    return (
      <table className="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Conference</th>
            </tr>
        </thead>
        <tbody>
            {attendees.map(attendee => {
                return (
                    <tr key={attendee.href}>
                        <td>{attendee.name}</td>
                        <td>{attendee.conference}</td>
                    </tr>
                );
            })}
        </tbody>
      </table>
    )
}

export default AttendeesList;
