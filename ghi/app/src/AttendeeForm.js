import {React, useEffect, useState } from 'react';

function AttendeeForm() {
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState("");
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");

    const handleOnChange = setFunction => {
        return (
            function (event) {
                setFunction(event.target.value);
            }
        )
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const postURL = "http://localhost:8001/api/attendees/";
        const data = {};
        data.conference = conference;
        data.name = fullName;
        data.email = email;

        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        };

        const postResponse = await fetch(postURL, fetchOptions);
        if (postResponse.ok){
            console.log("got it");
            const newPost = await postResponse.json();
            console.log(newPost);
            document.getElementById("create-attendee-form").reset();
        }
    }

    const fetchConferences = async () => {
        const confURL = 'http://localhost:8000/api/conferences/';
        const confRequest = await fetch(confURL);
        if (confRequest.ok){
            const data = await confRequest.json();
            setConferences(data.conferences);
        }
    }

    useEffect(() => {
        fetchConferences();
    }, []);

    let spinnerClass = "d-flex justify-content-center mb-3";
    let dropDownClass = "form-select d-none";
    if (conferences.length > 0){
        spinnerClass = "d-flex justify-content-center mb-3 d-none";
        dropDownClass = "form-select";
    }

    return (
    <div className="container">
        <div className="my-5">
        <div className="row">
            <div className="col col-sm-auto">
            <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" />
            </div>
            <div className="col">
            <div className="card shadow">
                <div className="card-body">
                <form onSubmit={handleSubmit} id="create-attendee-form">
                    <h1 className="card-title">It's Conference Time!</h1>
                    <p className="mb-3">
                    Please choose which conference
                    you'd like to attend.
                    </p>
                    <div className={spinnerClass} id="loading-conference-spinner">
                    <div className="spinner-grow text-secondary" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleOnChange(setConference)} name="conference" id="conference" className={dropDownClass} required>
                        <option value="">Choose a conference</option>
                        {conferences.map(conference => {
                            return (
                                <option value={conference.href} key={conference.href}>
                                    {conference.name}
                                </option>
                            );
                        })}
                    </select>
                    </div>
                    <p className="mb-3">
                        Now, tell us about yourself.
                    </p>
                    <div className="row">
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleOnChange(setFullName)} required placeholder="Your full name" type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Your full name</label>
                        </div>
                    </div>
                    <div className="col">
                        <div className="form-floating mb-3">
                            <input onChange={handleOnChange(setEmail)} required placeholder="Your email address" type="email" id="email" name="email" className="form-control" />
                            <label htmlFor="email">Your email address</label>
                        </div>
                    </div>
                    </div>
                    <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                <div className="alert alert-success d-none mb-0" id="success-message">
                    Congratulations! You're all signed up!
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    )
}

export default AttendeeForm;
