import {React, useEffect, useState } from 'react';

function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [Pname, setPname] = useState("");
    const [Cname, setCname] = useState("");
    const [Pemail, setPemail] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conference, setConference] = useState("");

    const handleOnChange = (setFunction) => {
        return (
            function (event) {
                setFunction(event.target.value)
            }
        );
    }

    const handleSubmit = async event => {
        event.preventDefault();
        const data = {};
        data.presenter_name = Pname;
        data.company_name = Cname;
        data.presenter_email = Pemail;
        data.title = title;
        data.synopsis = synopsis;

        const postURL = `http://localhost:8000${conference}presentations/`;
        const fetchOptions = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type":"application/json",
            }
        };

        const postResponse = await fetch(postURL, fetchOptions);
        if (postResponse.ok){
            console.log("got it");
            const newPost = await postResponse.json();
            console.log(newPost)
            document.getElementById("create-presentation-form").reset();
        }
    }

    const fetchConfData = async () => {
        const confURL = 'http://localhost:8000/api/conferences/';
        const confResponse = await fetch(confURL);
        if (confResponse.ok){
            const confData = await confResponse.json();
            setConferences(confData.conferences);
        }
    };

    useEffect(() => {
        fetchConfData();
    }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={handleSubmit} id="create-presentation-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleOnChange(setPname)} placeholder="Presenter Name" required type="text" id="presenter_name"  name="presenter_name" className="form-control" />
                                <label htmlFor="presenter_name">Presenter Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleOnChange(setCname)} placeholder="Company Name" type="text" id="company_name" name="company_name" className="form-control" />
                                <label htmlFor="company_name">Company Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleOnChange(setPemail)} placeholder="Presenter Email" required type="email" id="presenter_email" name="presenter_email" className="form-control" />
                                <label htmlFor="presenter_email">Presenter Email</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleOnChange(setTitle)} placeholder="Presentation Title" required type="text" id="title" name="title" className="form-control" />
                                <label htmlFor="title">Title</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="synopsis">Synopsis</label>
                                <textarea onChange={handleOnChange(setSynopsis)} required id="synopsis" name="synopsis" className="form-control" rows="3"></textarea>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleOnChange(setConference)} required id="conference" name="conference" className="form-select">
                                    <option value="">Choose a conference</option>
                                    {conferences.map(conference => {
                                        return (
                                            <option value={conference.href} key={conference.href}>
                                                {conference.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PresentationForm;
